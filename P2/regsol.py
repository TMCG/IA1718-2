import numpy as np
from sklearn import datasets, tree, linear_model
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import cross_val_score
import timeit

def mytraining(X,Y):
    value = bestRegression(bestValues(X,Y))
    return value[0]

def myprediction(X,reg):
    Ypred = reg.predict(X)
    return Ypred

def bestValues(X, Y):
    values = []
    auxiliarArray = []
    for i in range(2, 10):
        regtree = tree.DecisionTreeRegressor(min_samples_split=i)
        regtree = regtree.fit(X, Y)
        auxiliarArray.append((regtree, -cross_val_score( regtree, X, Y, cv = 5, scoring = 'neg_mean_squared_error').mean()))
    values.append(bestRegression(auxiliarArray))
    auxiliarArray = []
    for tp in ['rbf', 'polynomial']:
        for j in range(1,10):
            for i in range(1, 1000):
                regkr = KernelRidge(kernel=tp, gamma=0.1*j, alpha=0.001*i)
                regkr.fit(X,Y)
                auxiliarArray.append((regkr, -cross_val_score( regkr, X, Y, cv = 5, scoring = 'neg_mean_squared_error').mean()))
    values.append(bestRegression(auxiliarArray))
    return values

def bestRegression(array):
    value = array[0][1]
    bestTup = array[0]
    for tup in array:
        if tup[1] < value:
            bestTup = tup
    return bestTup