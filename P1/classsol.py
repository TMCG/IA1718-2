import numpy as np
from sklearn import neighbors, datasets, tree, linear_model

from sklearn.externals import joblib
import timeit

from sklearn.model_selection import cross_val_score

def features(X):

    F = np.zeros((len(X),5))
    for x in range(0,len(X)):
        F[x,0] = len(X[x])
        F[x,1] = nVowels(X[x])
        F[x,2] = nAccents(X[x])
        F[x,3] = nA(X[x])
        F[x,4] = nO(X[x])

    return F

def mytraining(f,Y):
    value = bestValues(f, Y)
    return value[0]
   

def myprediction(f, clf):
    Ypred = clf.predict(f)
    return Ypred

def nVowels(word):
    vowels = 'AEIOUaeiou'
    counter = 0
    for char in word:
        if char in vowels:
            counter += 1
    return counter

def nAccents(word):
    accents = "áéíóúãõàèìòùçâêîôûÁÉÍÓÚÃÕÀÈÌÒÙÇ"
    counter = 0
    for char in word:
        if char in accents:
            counter += 1
    return counter

def nA(word):
    a = "aáàãâäAÁÀÃÂÄ"
    counter = 0
    for char in word:
        if char in a:
            counter += 1
    return counter

def nO(word):
    o = "oóöòôõOÓÖÒÔÕ"
    counter = 0
    for char in word:
        if char in o:
            counter += 1
    return counter

def bestValues(f, Y):
    values = []
    KNeighborsClassifier = ['uniform', 'distance']
    for j in KNeighborsClassifier:
        auxiliarArray = []
        for i in range(1, 50):
            clfk = neighbors.KNeighborsClassifier(i, weights=j)
            clfk = clfk.fit(f, Y)
            Ypred = myprediction(f, clfk)
            auxiliarArray.append((clfk, np.sum(Y^Ypred)/len(f)))
        values.append(bestValuesAux(auxiliarArray))
    auxiliarArray = []
    for i in range(2, 50):
        clftree = tree.DecisionTreeClassifier(min_samples_split=i)
        clftree = clftree.fit(f, Y)
        Ypred = myprediction(f, clftree)
        auxiliarArray.append((clftree, np.sum(Y^Ypred)/len(f)))
        values.append(bestValuesAux(auxiliarArray))
    return bestValuesAux(values)

def bestValuesAux(array):
    value = 100
    bestTup = ()
    for tup in array:
        if tup[1] < value:
            value = tup[1]
            bestTup = tup
    return bestTup
